import React from 'react'
import './Nav.css'
import logo from './favicon-32x32.png'

function Nav() {
  return (
    <div className="Nav-main">
      <header className="Nav-header">
        <img src={logo} />
        <p>
          Fix-it-App
        </p>
      </header>
      <div className="dropdown Nav-list">
        <button className="dropbtn">
          General Maintenance
        </button>
          <div className="dropdown-content">
            <a
              className="Nav-link"
              href="https://reactjs.org"
            // target="_blank"
            // rel="noopener noreferrer"
            >
              Cleaning
            </a>
            <a
              className="Nav-link"
              href="https://reactjs.org"
            // target="_blank"
            // rel="noopener noreferrer"
            >
              Drywall
            </a>
            <a
              className="Nav-link"
              href="https://reactjs.org"
            // target="_blank"
            // rel="noopener noreferrer"
            >
              HVAC
            </a>
            <a
              className="Nav-link"
              href="https://reactjs.org"
            // target="_blank"
            // rel="noopener noreferrer"
            >
              Plumbing
            </a>
          </div>
      </div>
    </div>
  )
}

export default Nav
