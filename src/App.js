import React from 'react'
import './App.css'
import logo from './android-chrome-192x192.png'
import Nav from './Nav'

function App() {
  return (
    <div className="App">
      <div className="App-main">
        <header className="App-header">
          <img src={logo} />
          <p>
            Need general maintenance or cleaning?
        </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Order Now
        </a>
        </header>
      </div>
      <Nav />
    </div>
  )
}

export default App
